package com.fivvy.disclaimer.controller;

import com.fivvy.disclaimer.entity.Disclaimer;
import com.fivvy.disclaimer.repository.DisclaimerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/disclaimers")
public class DisclaimerController {
    private final DisclaimerRepository disclaimerRepository;

    @Autowired
    public DisclaimerController(DisclaimerRepository disclaimerRepository) {
        this.disclaimerRepository = disclaimerRepository;
    }

    @GetMapping
    public List<Disclaimer> getAllDisclaimers(@RequestParam(required = false) String text) {
        if (text != null) {
            return disclaimerRepository.findByTextContainingIgnoreCase(text);
        }
        return disclaimerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Disclaimer getDisclaimerById(@PathVariable String id) {
        return disclaimerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid disclaimer ID: " + id));
    }

    @PostMapping
    public Disclaimer createDisclaimer(@RequestBody Disclaimer disclaimer) {
        return disclaimerRepository.save(disclaimer);
    }

    @PutMapping("/{id}")
    public Disclaimer updateDisclaimer(@PathVariable String id, @RequestBody Disclaimer updatedDisclaimer) {
        Disclaimer disclaimer = disclaimerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid disclaimer ID: " + id));

        disclaimer.setName(updatedDisclaimer.getName());
        disclaimer.setText(updatedDisclaimer.getText());
        disclaimer.setVersion(updatedDisclaimer.getVersion());
        disclaimer.setUpdateAt(updatedDisclaimer.getUpdateAt());

        return disclaimerRepository.save(disclaimer);
    }

    @DeleteMapping("/{id}")
    public void deleteDisclaimer(@PathVariable String id) {
        disclaimerRepository.deleteById(id);
    }
}
