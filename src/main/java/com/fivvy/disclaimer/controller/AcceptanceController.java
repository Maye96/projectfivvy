package com.fivvy.disclaimer.controller;

import com.fivvy.disclaimer.entity.Acceptance;
import com.fivvy.disclaimer.repository.AcceptanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/acceptances")
public class AcceptanceController {
    private final AcceptanceRepository acceptanceRepository;

    @Autowired
    public AcceptanceController(AcceptanceRepository acceptanceRepository) {
        this.acceptanceRepository = acceptanceRepository;
    }

    @PostMapping
    public Acceptance createAcceptance(@RequestBody Acceptance acceptance) {
        return acceptanceRepository.save(acceptance);
    }

    @GetMapping
    public List<Acceptance> getAllAcceptances(@RequestParam(required = false) Integer userId) {
        if (userId != null) {
            return acceptanceRepository.findByUserId(userId);
        }
        return acceptanceRepository.findAll();
    }
}
