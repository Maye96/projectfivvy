package com.fivvy.disclaimer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "acceptances")
public class Acceptance {
    @Id
    private String id;
    private String disclaimer_id;
    private Integer userId;

    @CreatedDate
    private LocalDateTime create_at;

    @Component
    public static class AcceptanceListener extends AbstractMongoEventListener<Acceptance> {
        private final MongoTemplate mongoTemplate;

        public AcceptanceListener(MongoTemplate mongoTemplate) {
            this.mongoTemplate = mongoTemplate;
        }

        @Override
        public void onBeforeConvert(BeforeConvertEvent<Acceptance> event) {
            Acceptance acceptance = event.getSource();
            if (acceptance.getCreate_at() == null) {
                acceptance.setCreate_at(LocalDateTime.now());
            }
        }
    }
}
