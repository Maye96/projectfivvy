package com.fivvy.disclaimer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "disclaimers")
public class Disclaimer {

    @Id
    private String id;
    private String name;
    private String text;
    private String version;

    @CreatedDate
    private Date createAt;

    private Date updateAt;

    @Component
    public static class DisclaimerListener extends AbstractMongoEventListener<Disclaimer> {
        private final MongoTemplate mongoTemplate;

        public DisclaimerListener(MongoTemplate mongoTemplate) {
            this.mongoTemplate = mongoTemplate;
        }

        @Override
        public void onBeforeConvert(BeforeConvertEvent<Disclaimer> event) {
            Disclaimer disclaimer = event.getSource();
            Date now = new Date();

            if (disclaimer.getCreateAt() == null) {
                disclaimer.setCreateAt(now);
            }

            disclaimer.setUpdateAt(now);
        }

        @Override
        public void onBeforeSave(BeforeSaveEvent<Disclaimer> event) {
            Disclaimer disclaimer = event.getSource();
            disclaimer.setUpdateAt(new Date());
        }
    }
}
