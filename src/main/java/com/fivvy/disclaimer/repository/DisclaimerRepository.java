
package com.fivvy.disclaimer.repository;

import com.fivvy.disclaimer.entity.Disclaimer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisclaimerRepository extends MongoRepository<Disclaimer, String> {
    List<Disclaimer> findByTextContainingIgnoreCase(String text);
}