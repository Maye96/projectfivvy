package com.fivvy.disclaimer.repository;

import com.fivvy.disclaimer.entity.Acceptance;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcceptanceRepository extends MongoRepository<Acceptance, Integer> {
    List<Acceptance> findByUserId(Integer userId);
}