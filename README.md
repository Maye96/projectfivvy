# ProjectFivvy

Create a simple API to manage disclaimer acceptance of terms and conditions using Spring Boot, Docker and MongoDB

> **Note:** This poject was created in Windows 10

## Requirements

- Spring Boot 3.0.1
- JDK 17.0.7
- Maven 3.9.2
- Docker 24.0.2
- Postman

### Dependencies

- Lombok
- Spring Data MongoDB
- Spring Web

## MongoDB Image and use

In this poject a MongoDB image was used

1. docker pull mongo
2. docker run -d --name mongo-on-docker -p 27017:27017 mongo

## How to create Docker image and use

1. .\mvnw install
2. docker build -t {image_name} .
3. docker run -d --name {container_name} -p 8080:8080 {image_name}

# How to run the project with Docker Image to test it without building the project

1. docker pull mongo
2. docker run -d --name mongo-on-docker -p 27017:27017 mongo
3. docker pull maye96/fivvy_image:v1
4. docker run -d --name container_fivvy_image -p 8080:8080 maye96/fivvy_image:v1

With this the API service is running in http://localhost:8080, you can test the endpoint with Postman

## How to probate endpoints

* You need Postman to test the endpoints
* You can test it with the collection 2.1.0 from the collectionPostman folder

### Load data Disclaimers

- POST /disclaimers
1. Execute endpoint POST http://localhost:8080/disclaimers
2. In Params > Body paste:
    {
        "name":"Disclaimers lev P1",
        "text":"disclaimars Prueba 1",
        "version": "1.0.1"
    }
3. Press Send
![Alt text](img/image.png)


### View data Disclaimers

- GET /disclaimers
1.  Execute endpoint GET http://localhost:8080/disclaimers
2.  Press Send
![Alt text](img/image-1.png)

### Filter by text Disclaimers

- GET /disclaimers?text={string}
1.  Execute endpoint GET http://localhost:8080/disclaimers?text=prueba
2.  Press Send
![Alt text](img/image-2.png)


### Load Data Aceeptance

- POST /acceptances
1. Execute endpoint POST http://localhost:8080/acceptances
2. In Params > Body paste:
    {
        "disclaimer_id": colocar id del Discleimer creado
        "userId": 1
    }
3. Press Send
![Alt text](img/image-3.png)

### View data Acceptances

- GET /acceptances
1.  Execute endpoint GET http://localhost:8080/acceptances
2.  Press Send
![Alt text](img/image-4.png)

### Filter by userID Aceeptance

- GET /acceptances?userId={int}
1.  Execute endpoint GET http://localhost:8080/acceptances?userId=1
2.  Press Send
![Alt text](img/image-5.png)